package serial;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.TooManyListenersException;

import model.CarData;

public class Communicator implements SerialPortEventListener {
    private CarData model;

    // for containing the ports that will be found
    private Enumeration ports = null;
    // map the port names to CommPortIdentifiers
    private HashMap<String, CommPortIdentifier> portMap = new HashMap<String, CommPortIdentifier>();

    // this is the object that contains the opened port
    private CommPortIdentifier selectedPortIdentifier = null;
    private SerialPort serialPort = null;

    // input and output streams for sending and receiving data
    private InputStream input = null;
    private OutputStream output = null;

    // just a boolean flag that i use for enabling
    // and disabling buttons depending on whether the program
    // is connected to a serial port or not
    private boolean bConnected = false;

    final static int NEW_LINE_ASCII = 10;
    final static int CARRIAGE_RETURN = 13;

    final static int TIMEOUT = 2000;
    final static int BAUDRATE = 115200;

    StringBuilder sb = new StringBuilder();

    public Communicator(CarData model) {
        this.model = model;
    }

    // search for all the serial ports
    // pre style="font-size: 11px;": none
    // post: adds all the found ports to a combo box on the GUI
    public void searchForPorts() {
        ports = CommPortIdentifier.getPortIdentifiers();

        while (ports.hasMoreElements()) {
            CommPortIdentifier curPort = (CommPortIdentifier) ports
                    .nextElement();

            // get only serial ports
            if (curPort.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                portMap.put(curPort.getName(), curPort);
            }
        }
    }

    // connect to the selected port in the combo box
    // pre style="font-size: 11px;": ports are already found by using the
    // searchForPorts
    // method
    // post: the connected comm port is stored in commPort, otherwise,
    // an exception is generated
    public void connect() {
        CommPort commPort = null;

        try {
        	

            // the method below returns an object of type CommPort
            commPort = selectedPortIdentifier.open("Emulator", TIMEOUT);
            // the CommPort object can be casted to a SerialPort object
            serialPort = (SerialPort) commPort;

            serialPort.setSerialPortParams(
            		BAUDRATE,
           		    SerialPort.DATABITS_8,
           		    SerialPort.STOPBITS_1,
           		    SerialPort.PARITY_NONE);
            // for controlling GUI elements
            setConnected(true);

            // logging
            System.out.println(selectedPortIdentifier.getName()
                    + " opened successfully.");
        } catch (PortInUseException e) {
            System.err.println(selectedPortIdentifier.getName()
                    + " is in use. (" + e.toString() + ")");

        } catch (Exception e) {
            System.err.println("Failed to open "
                    + selectedPortIdentifier.getName() + "(" + e.toString()
                    + ")");
        }
    }

    // method that can be called to send data
    // pre style="font-size: 11px;": open serial port
    // post: data sent to the other device
    public void writeData(String data) {
        try {
        	for (char c : data.toCharArray()) {
                output.write(c);
            }

            output.flush();

        } catch (Exception e) {
            System.err.println("Failed to write data. (" + e.toString() + ")");

        }
    }

    public void newLine(){
        try {
        //output.write(NEW_LINE_ASCII);
        output.write(CARRIAGE_RETURN);
        output.flush();
        } catch (Exception e) {
            System.err.println("Failed to write data. (" + e.toString() + ")");

        }
    }
    // open the input and output streams
    // pre style="font-size: 11px;": an open port
    // post: initialized input and output streams for use to communicate data
    public boolean initIOStream() {
        // return value for whether opening the streams is successful or not
        boolean successful = false;

        try {
            //
            input = serialPort.getInputStream();
            output = serialPort.getOutputStream();
            writeData("Initialising");
            newLine();
            successful = true;
            return successful;
        } catch (IOException e) {
            System.err.println("I/O Streams failed to open. (" + e.toString()
                    + ")");
            return successful;
        }
    }

    // starts the event listener that knows whenever data is available to be
    // read
    // pre style="font-size: 11px;": an open serial port
    // post: an event listener for the serial port that knows when data is
    // received
    public void initListener() {
        try {
            serialPort.addEventListener(this);
            serialPort.notifyOnDataAvailable(true);
        } catch (TooManyListenersException e) {
            System.err.println("Too many listeners. (" + e.toString() + ")");

        }
    }

    // disconnect the serial port
    // pre style="font-size: 11px;": an open serial port
    // post: closed serial port
    public void disconnect() {
        // Drop if no port in existence
        if (getConnected() != true)
            return;
        // close the serial port
        try {
            serialPort.removeEventListener();
            serialPort.close();
            // input.close();
            // output.close();
            setConnected(false);

            System.out.println("Disconnected.");

        } catch (Exception e) {
            System.err.println("Failed to close " + serialPort.getName() + "("
                    + e.toString() + ")");

        }
    }

    private void setConnected(boolean b) {
        bConnected = b;
    }

    public boolean getConnected() {
        return bConnected;
    }

    public HashMap<String, CommPortIdentifier> getPorts() {
        return portMap;
    }

    public void setSelectedPort(String port) {
        selectedPortIdentifier = portMap.get(port);
    }

    public CommPortIdentifier getSelectedPort() {
        if (selectedPortIdentifier == null)
            return null;
        return selectedPortIdentifier;
    }

    public Object[] getPortKeys() {
        return portMap.keySet().toArray();
    }

    @Override
    public void serialEvent(SerialPortEvent evt) {
        if (evt.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
            try {
                byte singleData = (byte) input.read();

                if (singleData != NEW_LINE_ASCII
                        && singleData != CARRIAGE_RETURN) {
                    sb.append((char) singleData);

                } else {
                    parseString(sb.toString());
                    sb = new StringBuilder();

                }
            } catch (Exception e) {
                System.err.println("Failed to read data. (" + e.toString()
                        + ")");

            }
        }
    }

    public void parseString(String request) {
	    if(request.equalsIgnoreCase("ctt")) {  //CABIN TEMP
	        writeData(model.getCabinTemp());
	        newLine();
	    } else if (request.trim().equalsIgnoreCase("btt")) { // BATTERY TEMP
	        writeData(model.getBatteryTemp());
	        newLine();
	    } else if (request.trim().equalsIgnoreCase("btl")) { // BATTERY TEMP
	        writeData(model.getBatteryLevel());
	        newLine();
	    } else if (request.trim().equalsIgnoreCase("css")) { // Car Speed
            writeData(model.getCarSpeed());
            newLine();
        } else if (request.trim().equalsIgnoreCase("sai")) { // Solar array input
            writeData(model.getSolarInput());
            newLine();
        } else if (request.trim().equalsIgnoreCase("tpp")) { // Tyre pressure
            writeData(model.getTyrePressure());
            newLine();
        } else if (request.trim().equalsIgnoreCase("mtt")) { // Motor temp
            writeData(model.getMotorTemp());
            newLine();
        } else if (request.trim().equalsIgnoreCase("ran")) { // Range left
            writeData(model.getRangeLeft());
            newLine();
        } else if (request.trim().equalsIgnoreCase("pwr")) { // Power usage
            writeData(model.getPowerUsage());
            newLine();
        } else if (request.trim().equalsIgnoreCase("bms")) { // BMS state
            writeData(model.getBMSState());
            newLine();
        } else if (request.trim().equalsIgnoreCase("gps")) { // GPS state
            writeData(model.getGPSState());
            newLine();
        } else if (request.trim().equalsIgnoreCase("com")) { // Comms state
            writeData(model.getCommsState());
            newLine();
        } else {
	        writeData(sb + " [Invalid]");
	        newLine();
	    }
	    
	}
}
