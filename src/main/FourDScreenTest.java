package main;

import gui.MainView;
import model.CarData;
import serial.Communicator;

public class FourDScreenTest {

    /**
     * @param args
     */
    public static void main(String[] args) {
        CarData model = new CarData();
        Communicator comms = new Communicator(model);
        @SuppressWarnings("unused")
        MainView view = new MainView(model, comms);
        
    }

}
