package model;

public class CarData implements CarDataInterface {

    /* Set Defaults */
    private volatile String cabinTemp = "30";
    private volatile String batteryTemp = "30";
    private volatile String carSpeed = "30";
    private volatile String batteryLevel = "30";
    private volatile String solarInput = "30";
    private volatile String tyrePressure = "30";
    private volatile String motorTemp = "30";
    private volatile String rangeLeft = "30";
    private volatile String powerUsage = "30";
    private volatile boolean BMSState = true;
    private volatile boolean GPSState = true;
    private volatile boolean commsState = true;
        
    @Override
    public void setCabinTemp(String value) {
        this.cabinTemp = value;

    }

    @Override
    public void setBatteryTemp(String value) {
        this.batteryTemp = value;

    }

    @Override
    public void setCarSpeed(String value) {
        this.carSpeed = value;

    }

    @Override
    public void setBatteryLevel(String value) {
        this.batteryLevel = value;

    }

    @Override
    public void setSolarInput(String value) {
        this.solarInput = value;

    }

    @Override
    public void setTyrePressure(String value) {
        this.tyrePressure = value;

    }

    @Override
    public void setMotorTemp(String value) {
        this.motorTemp = value;

    }

    @Override
    public void setRangeLeft(String value) {
        this.rangeLeft = value;

    }

    @Override
    public void setPowerUsage(String value) {
        this.powerUsage = value;

    }

    @Override
    public void setBMSState(boolean value) {
        this.BMSState = value;

    }

    @Override
    public void setGPSState(boolean value) {
        this.GPSState = value;

    }

    @Override
    public void setCommsState(boolean value) {
        this.commsState = value;

    }

    @Override
    public String getCabinTemp() {
        return this.cabinTemp;
    }

    @Override
    public String getBatteryTemp() {
        return this.batteryTemp;
    }

    @Override
    public String getCarSpeed() {
        return this.carSpeed;
    }

    @Override
    public String getBatteryLevel() {
        return this.batteryLevel;
    }

    @Override
    public String getSolarInput() {
        return this.solarInput;
    }

    @Override
    public String getTyrePressure() {
        return this.tyrePressure;
    }

    @Override
    public String getMotorTemp() {
        return this.motorTemp;
    }

    @Override
    public String getRangeLeft() {
        return this.rangeLeft;
    }

    @Override
    public String getPowerUsage() {
        return this.powerUsage;
    }

    @Override
    public String getBMSState() {
        return ((Boolean)this.BMSState).toString();
    }

    @Override
    public String getGPSState() {
        return ((Boolean)this.GPSState).toString();

    }

    @Override
    public String getCommsState() {
        return ((Boolean)this.commsState).toString();

    }

}
