package model;

public interface CarDataInterface {

    void setCabinTemp(String value);
    void setBatteryTemp(String value);
    void setCarSpeed(String value);
    void setBatteryLevel(String value);
    void setSolarInput(String value);
    void setTyrePressure(String value);
    void setMotorTemp(String value);
    void setRangeLeft(String value);
    void setPowerUsage(String value);
    void setBMSState(boolean value);
    void setGPSState(boolean value);
    void setCommsState(boolean value);
    
    String getCabinTemp();
    String getBatteryTemp();
    String getCarSpeed();
    String getBatteryLevel();
    String getSolarInput();
    String getTyrePressure();
    String getMotorTemp();
    String getRangeLeft();
    String getPowerUsage();
    String getBMSState();
    String getGPSState();
    String getCommsState();
}
