package gui;

import model.CarData;

public class Commands {

    private CarData model;

    public Commands(CarData cd) {
        this.model = cd;
    }

    public class CabinTempCommand implements Command {

        @Override
        public void update(String data) {
            model.setCabinTemp(data);
        }
    }

    public class BatteryTempCommand implements Command {

        @Override
        public void update(String data) {
            model.setBatteryTemp(data);
        }
    }

    public class CarSpeedCommand implements Command {

        @Override
        public void update(String data) {
            model.setCarSpeed(data);
        }
    }

    public class BatteryLevelCommand implements Command {

        @Override
        public void update(String data) {
            model.setBatteryLevel(data);
        }
    }

    public class SolarInputCommand implements Command {

        @Override
        public void update(String data) {
            model.setSolarInput(data);
        }
    }

    public class TyrePressureCommand implements Command {

        @Override
        public void update(String data) {
            model.setTyrePressure(data);
        }
    }

    public class MotorTempCommand implements Command {

        @Override
        public void update(String data) {
            model.setMotorTemp(data);
        }
    }

    public class RangeLeftCommand implements Command {

        @Override
        public void update(String data) {
            model.setRangeLeft(data);
        }
    }

    public class PowerUsageCommand implements Command {

        @Override
        public void update(String data) {
            model.setPowerUsage(data);
        }
    }

    public class BMSStateCommand implements Command {

        @Override
        public void update(String data) {
            if (data.compareTo("true") == 0)
                model.setBMSState(true);
            else
                model.setBMSState(false);
        }
    }

    public class GPSStateCommand implements Command {

        @Override
        public void update(String data) {
            if (data.compareTo("true") == 0)
                model.setGPSState(true);
            else
                model.setGPSState(false);
        }
    }
    
    public class CommsStateCommand implements Command {

        @Override
        public void update(String data) {
            if (data.compareTo("true") == 0)
                model.setCommsState(true);
            else
                model.setCommsState(false);
        }
    }

}
