package gui;

import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class CheckBoxPanel extends JPanel {
    private MainView view;
    private Command command;
    private JPanel sizePanel;
    private JCheckBox box;
    private JLabel label;
    
    public CheckBoxPanel(String name, MainView view, Command command){
        this.view = view;
        this.command = command;
        setLayout(new FlowLayout(SwingConstants.WEST, 5, 5));
        
        sizePanel = new JPanel();
        sizePanel.setLayout(new BoxLayout(sizePanel, BoxLayout.X_AXIS));
        
        label = new JLabel(name);
        box = new JCheckBox();
        
        box.addActionListener(new CheckBoxController(command, this));
        
        sizePanel.add(box);
        sizePanel.add(label);
        
        add(sizePanel);
        command.update(((Boolean)box.isSelected()).toString());
        
        
    }
    
    public Boolean getCheckBoxSelected(){
    	return (Boolean) box.isSelected();
    }
}
