package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import model.CarData;
import serial.Communicator;

@SuppressWarnings("serial")
public class MainView extends JFrame {
	private CarData model;
	private Communicator comms;
	private Commands commands;
	private Statusbar status;
	private Menubar menubar;
	private JPanel mainPanel;
	private JPanel checkBoxPanel;

	public MainView(CarData model, Communicator comms) {
		this.model = model;
		this.comms = comms;
		this.commands = new Commands(model);

		menubar = new Menubar(this);
		status = new Statusbar(this);
		mainPanel = new JPanel();
		checkBoxPanel = new JPanel();

		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

		mainPanel.add(new SliderPanel("Cabin Temp", -30, 80, this,
				commands.new CabinTempCommand()));
		mainPanel.add(new SliderPanel("Battery Temp", -30, 200, this,
				commands.new BatteryTempCommand()));
		mainPanel.add(new SliderPanel("Battery Level", 0, 100, this,
				commands.new BatteryLevelCommand()));
		mainPanel.add(new SliderPanel("Car Speed", -20, 150, this,
				commands.new CarSpeedCommand()));
		mainPanel.add(new SliderPanel("Solar Arrary Input", 0, 200, this,
				commands.new SolarInputCommand()));
		mainPanel.add(new SliderPanel("Tyre Pressure", 0, 50, this,
				commands.new TyrePressureCommand()));
		mainPanel.add(new SliderPanel("Motor Temp", -30, 200, this,
				commands.new MotorTempCommand()));
		mainPanel.add(new SliderPanel("Range Left", 0, 1000, this,
				commands.new RangeLeftCommand()));
		mainPanel.add(new SliderPanel("Power Usage", -100, 200, this,
				commands.new SolarInputCommand()));
		checkBoxPanel.add(new CheckBoxPanel("BMS State", this,
				commands.new BMSStateCommand()));
		checkBoxPanel.add(new CheckBoxPanel("GPS State", this,
				commands.new GPSStateCommand()));
		checkBoxPanel.add(new CheckBoxPanel("Comms State", this,
				commands.new CommsStateCommand()));
		mainPanel.add(checkBoxPanel);

		setTitle("Data Emulator");
		setSize(400, 520);

		setMinimumSize(new Dimension(400, 520));

		setLocationRelativeTo(null);
		setLayout(new BorderLayout());

		add(mainPanel);

		add(status, BorderLayout.SOUTH);
		add(menubar, BorderLayout.NORTH);

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}

	public Communicator getComms(){
	    return comms;
	}
	
	public CarData getModel() {
		return model;
	}
	
	/**
	 * Close all open ports and connections
	 */
	public void safeExit() {
	    // Clean up ports
	    System.exit(0);
	}
	
	public void getConfigDialog(){
		comms.searchForPorts();
		ConfigDialog cd = new ConfigDialog(this, comms.getPortKeys());
		cd.setVisible(true);
	}
}
