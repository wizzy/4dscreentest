package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class SliderController {

    private SliderPanel sp;
    private Command command;
    public SliderController(Command command, SliderPanel sp){
        this.sp = sp;
        this.command = command;
        
    }
    
    public ActionListener getTextBoxListener(){
        return new TextBoxListener();
    }
    
    public ChangeListener getSliderListener(){
        return new SliderListener();
    }
    
    private class SliderListener implements ChangeListener{

        @Override
        public void stateChanged(ChangeEvent e) {
            sp.updateTextBoxValue();
            command.update(sp.getSliderValue());
        }
        
    }

    private class TextBoxListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            sp.updateSliderValue();
            command.update(sp.getSliderValue());

        }
    }

}
