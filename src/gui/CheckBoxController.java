package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CheckBoxController implements ActionListener {

	private Command command;
	private CheckBoxPanel cbp;
	
	public CheckBoxController(Command command, CheckBoxPanel cbp){
		this.command = command;
		this.cbp = cbp;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		command.update(cbp.getCheckBoxSelected().toString());
	}

}
