package gui;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class SliderPanel extends JPanel {
    private MainView view;
    private Command command;
    private SliderController controller;
    private JPanel sizePanel;
    private JPanel rightPanel;
    private JSlider slider;
    private JLabel label;
    private JTextField text;
    private int min;
    private int max;
    public static int MIN = -30;
    public static int MAX = 150;
    public static int BOX_SIZE = 3;

    public SliderPanel(String name, MainView view, Command command) {
    	this(name, MIN, MAX, view, command);
    }
    
    public SliderPanel(String name, int min, int max, MainView view, Command command){
        this.view = view;
        this.command = command;
        this.controller = new SliderController(this.command, this);
        this.min = min;
        this.max = max;
        setLayout(new BorderLayout());

        sizePanel = new JPanel();
        rightPanel = new JPanel();
        label = new JLabel(name);
        
      
        slider = new JSlider(SwingConstants.HORIZONTAL, min, max, (max + min) / 2 );
        if(min <0)
        	slider.setMajorTickSpacing((max - min));
        else
        	slider.setMajorTickSpacing(max+min);
        slider.setPaintLabels(true);
        
        text = new JTextField(((Integer) ((max + min) / 2)).toString(), BOX_SIZE);

        slider.addChangeListener(controller.getSliderListener());
        text.addActionListener(controller.getTextBoxListener());
        
        sizePanel.add(label);
        rightPanel.add(slider);
        rightPanel.add(text);

        add(sizePanel, BorderLayout.WEST);
        add(rightPanel, BorderLayout.EAST);
    	
        command.update(getSliderValue());
    }

    public String getSliderValue(){
        return ((Integer)slider.getValue()).toString();
    }
    public void updateTextBoxValue() {
        text.setText(((Integer)slider.getValue()).toString());
    }

    /**
     * Updates slider value from textbox.
     * 
     * THIS IS NOT YET SAFE.
     */
    public void updateSliderValue() {
        int value;
        try{
            value = Integer.parseInt(text.getText());
        } catch (NumberFormatException e){
            value = (max + min)/2;
            text.setText(((Integer)value).toString());
            e.printStackTrace();            
        }
        
        /* Set to existing if number parsing fails */
        slider.setValue(value);
     
    }

}
