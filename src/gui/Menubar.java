package gui;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

@SuppressWarnings("serial")
public class Menubar extends JMenuBar{

    private MainView view;
    private MenuController controller;
    private JMenu main = new JMenu("File");
    private JMenu settings = new JMenu("Settings");
    private JMenuItem connect = new JMenuItem("Connect");
    private JMenuItem disconnect = new JMenuItem("Disconnect");
    private JMenuItem exit = new JMenuItem("Exit");
    private JMenuItem portConf = new JMenuItem("Configure Serial");
    
    
    public Menubar(MainView view) {
        this.view = view;
        this.controller = new MenuController(view);
        
        connect.addActionListener(controller.getMenuConnectController());
        disconnect.addActionListener(controller.getMenuDisconnectController());
        exit.addActionListener(controller.getMenuExitController());
        
        portConf.addActionListener(controller.getMenuConfigController());
        
        main.add(connect);
        main.add(disconnect);
        main.addSeparator();
        main.add(exit);

        settings.add(portConf);
        
        add(main);
        
        add(settings);
    }

}
