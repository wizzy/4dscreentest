package gui;

public interface Command {
    void update(String data);
}
