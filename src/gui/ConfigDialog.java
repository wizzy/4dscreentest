package gui;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.security.auth.login.FailedLoginException;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JPanel;

import sun.java2d.pipe.hw.AccelDeviceEventListener;

@SuppressWarnings("serial")
public class ConfigDialog extends JDialog {
	private MainView view;
	private JComboBox portBox;
	private JPanel main = new JPanel();
	private JPanel combo = new JPanel();
	private JPanel buttons = new JPanel();
	private JButton ok = new JButton("Ok");
	private JButton cancel = new JButton("cancel");
	private String selectedPort = null;
	
	public ConfigDialog(MainView view, Object[] ports) {
		super(view, "Configuration Window", false);
		this.view = view;
		
		portBox = new JComboBox(ports);
		
		main.setLayout(new BoxLayout(main, BoxLayout.Y_AXIS));
		combo.setLayout(new FlowLayout());
		buttons.setLayout(new BoxLayout(buttons, BoxLayout.X_AXIS));
		cancel.addActionListener(new cancelListener());
		ok.addActionListener(new okListener());
		
		if(view.getComms().getSelectedPort() != null)
			portBox.setSelectedItem(view.getComms().getSelectedPort().getName());
		
		add(main);
		combo.add(portBox);
		main.add(combo);
		buttons.add(ok);
		buttons.add(cancel);
		main.add(buttons);
		setSize(250, 120);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(view);

	}
	
	public class cancelListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			setVisible(false);
		}
		
	}
	
	public class okListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			view.getComms().setSelectedPort((String) portBox.getSelectedItem());
			setVisible(false);

		}
		
	}

}
