package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.CarData;

public class MenuController {
    
    private MainView view;
    private CarData model;
    
    public MenuController(MainView view){
        this.view = view;
        model = view.getModel();
        
    }

    public MenuExitController getMenuExitController(){
        return new MenuExitController();
    }
    
    public MenuDisconnectController getMenuDisconnectController(){
        return new MenuDisconnectController();
    }
    
    public MenuConnectController getMenuConnectController(){
        return new MenuConnectController();
    }
    public MenuConfigController getMenuConfigController(){
        return new MenuConfigController();
    }
    
    public class MenuExitController implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            view.safeExit();
        }
        
    }
    public class MenuConnectController implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            view.getComms().connect();
            if (view.getComms().getConnected() == true)
            {
                if (view.getComms().initIOStream() == true)
                {
                	view.getComms().initListener();
                }
            }
        }
        
    }
    public class MenuDisconnectController implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
        	view.getComms().disconnect();
        }
        
    }
    public class MenuConfigController implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
        	view.getConfigDialog();
        }
        
    }
}
