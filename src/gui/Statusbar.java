package gui;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public class Statusbar extends JLabel{

    MainView view;
    
    public Statusbar(MainView view) {
        super("Serial connection data to be here!");
        this.view = view;

        setBorder(BorderFactory.createLoweredBevelBorder());
    }

    
    public void update(String text) {

    }

}
